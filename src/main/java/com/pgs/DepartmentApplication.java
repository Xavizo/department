package com.pgs;
import com.pgs.database.entities.DepartmentRepository;
import com.pgs.database.entities.EmployeeRepository;
import com.pgs.database.entities.DepartmentEntity;
import com.pgs.database.entities.EmployeeEntity;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import java.math.BigDecimal;


@SpringBootApplication
public class DepartmentApplication {

    private static final Logger log = LoggerFactory.getLogger(DepartmentApplication.class);


    PropertyMap<DepartmentEntity, DepartmentDTO> departmentMap = new PropertyMap<DepartmentEntity, DepartmentDTO>() {
        @Override
        protected void configure() {
            map().setId(source.getDepartmentId());
            map().setName(source.getName());
            map().setCity(source.getCity());

        }
    };

    PropertyMap<EmployeeEntity,EmployeeDTO> employeeMap = new PropertyMap<EmployeeEntity, EmployeeDTO>() {
        @Override
        protected void configure() {
            map().setId(source.getEmployeeId());
            map().setName(source.getFirstName());
            map().setAddress(source.getAddress());
            map().setPhoneNumber(source.getPhone());
            map().setSalary(source.getSalary());
            map().setDepartmentName(source.getDepartment().getName());
            map().setDepartmentCity(source.getDepartment().getCity());

        }
    };
	public static void main(String[] args) {

	    SpringApplication.run(DepartmentApplication.class, args);
//        DepartmentEntity d1 = new DepartmentEntity(new Long("4"), "Aseco", "rzeszow", new BigDecimal("1000"), new BigDecimal("200000"));
//        EmployeeEntity e1 = new EmployeeEntity(new Long("3"), new BigDecimal("2500"), "Marta", "Kot", "Lubaczow Mickiewicza 4", "48335131222");
//        e1.setFirstName("wafel");
//        e1.setDepartment(d1);
//        //e1.setDepartment(d1);
//        System.out.println("Tutaj powinien byc e1");
//        System.out.println(e1.toString());
	}

	@Bean
    public CommandLineRunner demo(DepartmentRepository repository, EmployeeRepository repository1){
        return (args) -> {

//            //inserting data to department_entity
//            repository.deleteAll();
//            repository.save(new DepartmentEntity(new Long("1"), "PGS-software", "Rzeszow", new BigDecimal("2100"), new BigDecimal("13500")));
//            repository.save(new DepartmentEntity(new Long("2"), "ASECO", "Rzeszow", new BigDecimal("1300"), new BigDecimal("42000")));
//            repository.save(new DepartmentEntity(new Long("3"), "X-KOM", "Rzeszow", new BigDecimal("1300"), new BigDecimal("5300")));
//            repository.save(new DepartmentEntity(new Long("4"), "PocztaPolska", "Krakow", new BigDecimal("1200"), new BigDecimal("3000")));
//            repository.save(new DepartmentEntity(new Long("5"), "Skarbowka", "Rzeszow", new BigDecimal("3000"), new BigDecimal("9000")));
//            repository.save(new DepartmentEntity(new Long("6"), "Urzad gminy", "Lubaczow", new BigDecimal("3000"), new BigDecimal("6800")));
//            repository.save(new DepartmentEntity(new Long("7"), "Sklep GRACJA", "Lubaczow", new BigDecimal("1500"), new BigDecimal("4000")));
//            repository.save(new DepartmentEntity(new Long("8"), "Jubiler", "Lubaczow", new BigDecimal("1500"), new BigDecimal("4500")));
//            repository.save(new DepartmentEntity(new Long("9"), "Fryzjer", "Lubaczow", new BigDecimal("1200"), new BigDecimal("2200")));
//            repository.save(new DepartmentEntity(new Long("10"), "Fryzjer", "Rzeszow", new BigDecimal("1400"), new BigDecimal("3000")));
//            repository.save(new DepartmentEntity(new Long("11"), "Skarbowka", "Warszawa", new BigDecimal("3500"), new BigDecimal("5600")));
//            repository.save(new DepartmentEntity(new Long("12"), "X-KOM", "Warszawa", new BigDecimal("1400"), new BigDecimal("4500")));
//            repository.save(new DepartmentEntity(new Long("13"), "PGS-software", "Gdansk", new BigDecimal("2000"), new BigDecimal("5000")));
//            repository.save(new DepartmentEntity(new Long("14"), "PocztaPolska", "Rzeszow", new BigDecimal("2000"), new BigDecimal("4000")));
//
//            //inserting data to employee_entity
//            repository1.save(new EmployeeEntity(new Long("1"), new BigDecimal("2000"), "Michal", "Kichal", "Rzeszow Zagloby 8", "48535030222"));
//            repository1.save(new EmployeeEntity(new Long("2"), new BigDecimal("3000"), "Kamil", "Kamilowy", "Jaroslaw Podzamcze 11", "48435346456"));
//            repository1.save(new EmployeeEntity(new Long("3"), new BigDecimal("2500"), "Marta", "Kot", "Lubaczow Mickiewicza 4", "48335131222"));
//            repository1.save(new EmployeeEntity(new Long("4"), new BigDecimal("5000"), "Mateusz", "Mati", "tests", "testse"));
//            repository1.save(new EmployeeEntity(new Long("5"), new BigDecimal("3000"), "Karol", "Karolczak", "tests", "testse"));
//            repository1.save(new EmployeeEntity(new Long("6"), new BigDecimal("5100"), "Andrzej", "Duda", "tests", "tests"));
//            repository1.save(new EmployeeEntity(new Long("7"), new BigDecimal("7000"), "Michal", "Brodzinski", "tests", "tests"));
//            repository1.save(new EmployeeEntity(new Long("8"), new BigDecimal("11000"), "Kamil", "Sep", "tests", "tests"));
//            repository1.save(new EmployeeEntity(new Long("9"), new BigDecimal("10000"), "Karol", "Potakiewicz", "tests", "tests"));
//            repository1.save(new EmployeeEntity(new Long("10"), new BigDecimal("5000"), "Andrzej", "Ziemba", "testse", "testse"));
//            repository1.save(new EmployeeEntity(new Long("11"), new BigDecimal("20000"), "Iza", "Wylew", "tsetse", "testse"));
//            repository1.save(new EmployeeEntity(new Long("12"), new BigDecimal("13000"), "Alojza", "Ziemkiewicz", "testse", "testse"));
//            repository1.save(new EmployeeEntity(new Long("13"), new BigDecimal("1200"), "Marcin", "Kaminski", "testsets", "testse"));
//            repository1.save(new EmployeeEntity(new Long("14"), new BigDecimal("1500"), "Michal", "Klich", "testse", "testse"));
//            repository1.save(new EmployeeEntity(new Long("15"), new BigDecimal("4000"), "Albert", "Kaczynski", "testse", "testse"));
//            repository1.save(new EmployeeEntity(new Long("16"), new BigDecimal("3000"), "Hanna", "Walec", "testse", "testse"));
//            repository1.save(new EmployeeEntity(new Long("17"), new BigDecimal("2500"), "Eryk", "Gancarz", "testse", "tests"));
//            repository1.save(new EmployeeEntity(new Long("18"), new BigDecimal("6700"), "Krzysiek", "Fryc", "testse", "testse"));
//            repository1.save(new EmployeeEntity(new Long("19"), new BigDecimal("3000"), "Piotr", "Karkut", "tsetse", "testse"));
//
//
//
//            log.info("Departments found with findAll():");
//            for(DepartmentEntity departmentEntity : repository.findAll()){
//                log.info(departmentEntity.toString());
//            }
//
//            log.info("Employees found with findAll():");
//            for(EmployeeEntity employeeEntity : repository1.findAll()){
//                log.info(employeeEntity.toString());
//            }


        };
    }
}




