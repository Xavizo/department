package com.pgs.database.entities;

import org.hibernate.annotations.SQLDelete;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;


import java.sql.ResultSet;
import java.util.List;


@Repository
    public interface DepartmentRepository extends JpaRepository<DepartmentEntity, Long> {
        List<DepartmentEntity> findByName(String name);

        List<DepartmentEntity> getByDepartmentId(Long departmentId);


    @Modifying
    @Transactional
    @Query("DELETE FROM DepartmentEntity d WHERE d = ?1 AND d NOT IN ( SELECT e FROM EmployeeEntity e)")
    void deleteDepartment(DepartmentEntity department);


    }
