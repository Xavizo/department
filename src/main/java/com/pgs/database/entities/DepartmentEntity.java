package com.pgs.database.entities;
import javax.persistence.*;
import java.math.BigDecimal;


@Entity
public class DepartmentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long departmentId;
    private String name;
    private String city;
    private BigDecimal minSalary;
    private BigDecimal maxSalary;

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public BigDecimal getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(BigDecimal minSalary) {
        this.minSalary = minSalary;
    }

    public BigDecimal getMaxSalary() {
        return maxSalary;
    }

    public void BigDecimal(BigDecimal maxSalary) {
        this.maxSalary = maxSalary;
    }


    public DepartmentEntity(){
        super();
    }
    public DepartmentEntity(Long departmentId, String name, String city, BigDecimal minSalary, BigDecimal maxSalary) {
        this.departmentId = departmentId;
        this.name = name;
        this.city = city;
        this.minSalary = minSalary;
        this.maxSalary = maxSalary;
    }


    @Override
    public String toString() {
        return "DepartmentEntity{" +
                "departmentId=" + departmentId +
                ", name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", minSalary=" + minSalary +
                ", maxSalary=" + maxSalary +
                '}';
    }
}
