package com.pgs.database.entities;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeEntity, Long> {
    List<EmployeeEntity> findByFirstName(String firstName);

    @Modifying
    @Transactional
    @Query("update EmployeeEntity u set u.department =?2 where u =?1")
    void updateDepartment(EmployeeEntity employee, DepartmentEntity department);

    //zatrudnieni
    @Transactional
    //@Query("select d, e from EmployeeEntity e, DepartmentEntity d where e.department = d.departmentId")
    @Query("select d.name, avg(d.maxSalary + d.minSalary) from EmployeeEntity e, DepartmentEntity d where e.department = d.departmentId group by d.name")
    List<EmployeeEntity> avgDepartmentsSalary();

    @Query("select d.city, avg(d.maxSalary + d.minSalary) from EmployeeEntity e, DepartmentEntity d where e.department = d.departmentId group by d.city")
    List<EmployeeEntity> avgCitySalary();


}