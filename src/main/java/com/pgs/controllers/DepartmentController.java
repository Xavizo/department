package com.pgs.controllers;

import com.pgs.database.entities.DepartmentRepository;
import com.pgs.database.entities.DepartmentEntity;
import com.pgs.database.entities.EmployeeEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Controller
    @RequestMapping("/departments")
    public class DepartmentController {

        @Autowired
        private DepartmentRepository repository;

        @Autowired
       private ModelMapper modelMapper;


    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Collection<DepartmentEntity>> getAllUsers(@RequestParam(required = false) String column,
                                                                  @RequestParam(required = false) String direct) {

        List<DepartmentEntity> list = new ArrayList<>();
        if (!(column == null || column.equals("")
                || direct == null || direct.equals(""))) {
            Sort sort = new Sort(Sort.Direction.fromString(direct), column);
            list.addAll(repository.findAll(sort));
        } else {
            list.addAll(repository.findAll());
        }

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

        @RequestMapping(value="/{id}",method = RequestMethod.GET)
        public ResponseEntity<DepartmentEntity> getByDepartmentId(@PathVariable("id") Long departmentId){
        return new ResponseEntity<>(repository.findOne(departmentId),HttpStatus.OK);
        }



        @RequestMapping(method = RequestMethod.POST)
        public ResponseEntity<DepartmentEntity> addDepartment(@RequestBody DepartmentEntity department){
            return new ResponseEntity<>(repository.save(department),HttpStatus.CREATED);
        }



    @RequestMapping(value="/{departmentId}",method=RequestMethod.DELETE)
        public ResponseEntity<?> removeDepartment (@PathVariable Long departmentId) throws Exception{

        try {
            repository.deleteDepartment(repository.findOne(departmentId));
            //System.out.println("Udalo sie usunac!");
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch(Exception e){

            //String wyjatek=" You can't remove department with employees ";
            //System.out.println(e+wyjatek);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        }


    @RequestMapping(value="/{name}",method=RequestMethod.GET)
    public ResponseEntity<Collection<DepartmentEntity>> findByName (@PathVariable String name){
        return new ResponseEntity<>(repository.findByName(name),HttpStatus.OK);
    }


    @RequestMapping(value="/user",method = RequestMethod.PUT)
    public ResponseEntity<DepartmentEntity> updateDepartment(@RequestBody DepartmentEntity department){
        return new ResponseEntity<>(repository.save(department),HttpStatus.OK);
    }

}



