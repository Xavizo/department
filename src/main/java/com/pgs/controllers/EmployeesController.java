package com.pgs.controllers;

import com.pgs.database.entities.DepartmentRepository;
import com.pgs.database.entities.EmployeeRepository;
import com.pgs.database.entities.EmployeeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import java.util.*;


@Controller
@RequestMapping("/employees")
public class EmployeesController {

    @Autowired
    private EmployeeRepository repository;
    @Autowired
    private DepartmentRepository departmentRepository;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Collection<EmployeeEntity>> getAllUsers(@RequestParam(required = false) String column,
                                                                  @RequestParam(required = false) String direct) {
        if (!(StringUtils.isBlank(column) || StringUtils.isBlank(direct))) {
            Sort sort = new Sort(Sort.Direction.fromString(direct), column);
            return new ResponseEntity<>(repository.findAll(sort), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(repository.findAll(), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/rel", method = RequestMethod.PUT)
    public ResponseEntity<?> example(@RequestParam(value = "department") Long departmentId,
                                     @RequestParam(value = "employee") Long employeeId) {
        if (repository.findOne(employeeId) != null && departmentRepository.findOne(departmentId) != null) {
            repository.updateDepartment(repository.findOne(employeeId), departmentRepository.findOne(departmentId));
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/avg", method = RequestMethod.GET)
    public ResponseEntity<Collection<EmployeeEntity>> getSalary(@RequestParam("salary") String type) {

        if (type.equals("department")) {
            return new ResponseEntity<Collection<EmployeeEntity>>(repository.avgDepartmentsSalary(), HttpStatus.OK);
        } else if (type.equals("city")) {
            return new ResponseEntity<Collection<EmployeeEntity>>(repository.avgCitySalary(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.GET, params = {"name"})
    public ResponseEntity<Collection<EmployeeEntity>> searchUser(@RequestParam(value = "name") String name) {
        return new ResponseEntity<>(repository.findByFirstName(name), HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public ResponseEntity<EmployeeEntity> getByDepartmentId(@PathVariable("id") Long employeeId) {
        return new ResponseEntity<>(repository.findOne(employeeId), HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public ResponseEntity<EmployeeEntity> removeUser(@PathVariable("id") Long employeeId) {
        repository.delete(employeeId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/user", method = RequestMethod.PUT)
    public ResponseEntity<?> updateUser(@RequestBody EmployeeEntity employee) {
        return new ResponseEntity<>(repository.save(employee), HttpStatus.OK);
    }

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public ResponseEntity<?> createUser(@RequestBody EmployeeEntity employee) {
        return new ResponseEntity<>(repository.save(employee), HttpStatus.CREATED);
    }
}
