package com.pgs.data;

/**
 * Created by jryndak on 9/2/2016.
 */
public class DepartmentDTO {

    private Long id;
    private String name;
    private String city;
    private int employeesCount;


    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public int getEmployeesCount() {
        return employeesCount;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setEmployeesCount(int employeesCount) {
        this.employeesCount = employeesCount;
    }


    public DepartmentDTO(Long id, String name, String city, int employeesCount) {
        this.id = id;
        this.name = name;
        this.city = city;
        this.employeesCount = employeesCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DepartmentDTO)) return false;

        DepartmentDTO that = (DepartmentDTO) o;

        if (getEmployeesCount() != that.getEmployeesCount()) return false;
        if (getId() != null ? !getId().equals(that.getId()) : that.getId() != null) return false;
        if (getName() != null ? !getName().equals(that.getName()) : that.getName() != null) return false;
        return getCity() != null ? getCity().equals(that.getCity()) : that.getCity() == null;

    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getCity() != null ? getCity().hashCode() : 0);
        result = 31 * result + getEmployeesCount();
        return result;
    }
}
