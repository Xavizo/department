package com.pgs.data;

import com.pgs.database.entities.DepartmentEntity;
import com.pgs.database.entities.EmployeeEntity;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;


/**
 * Created by jryndak on 9/9/2016.
 */
public class convertToDTO {

//    PropertyMap<DepartmentEntity, DepartmentDTO> departmentMap = new PropertyMap<DepartmentEntity, DepartmentDTO>() {
//        @Override
//        protected void configure() {
//            map().setId(source.getDepartmentId());
//            map().setName(source.getName());
//            map().setCity(source.getCity());
//            map().setEmployeesCount(source.getEmployeesCount);
//
//        }
//    };
//
//    PropertyMap<EmployeeEntity,EmployeeDTO> employeeMap = new PropertyMap<EmployeeEntity, EmployeeDTO>() {
//        @Override
//        protected void configure() {
//            map().setId(source.getEmployeeId());
//            map().setName(source.getFirstName());
//            map().setAddress(source.getAddress());
//            map().setPhoneNumber(source.getPhone());
//            map().setSalary(source.getSalary());
//            map().setDepartmentName(source.getDepartment().getName());
//            map().setDepartmentCity(source.getDepartment().getCity());
//
//        }
//    };


    private ModelMapper employeeModelMapper;
    private ModelMapper departmentModelMapper;




    private DepartmentDTO convertToDepartmentDTO(DepartmentEntity department){
        DepartmentDTO departmentDto = departmentModelMapper.map(department, DepartmentDTO.class);

//
//        department.getDepartmentId();
//        department.getName();
//        department.getCity();

        departmentDto.getId();
        departmentDto.getName();
        departmentDto.getCity();



        return departmentDto;
    }

    private EmployeeDTO  convertToEmployeeDTO(EmployeeEntity employee){
        EmployeeDTO employeeDto = employeeModelMapper.map(employee, EmployeeDTO.class);

        employeeDto.getId();
        employeeDto.getName();
        employeeDto.getAddress();
        employeeDto.getPhoneNumber();
        employeeDto.getSalary();
        employeeDto.getDepartmentName();
        employeeDto.getDepartmentCity();

        return employeeDto;
    }

    public DepartmentDTO getConvertDep(DepartmentEntity dep){
        return convertToDepartmentDTO(dep);
    }

    public EmployeeDTO getConvertEmp(EmployeeEntity emp){
        return convertToEmployeeDTO(emp);
    }
}

